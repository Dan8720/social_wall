//////////
// Card //
//////////
///
// This will contain the card components

import React, { Component } from "react";
import PropTypes from "prop-types";
import * as twitter from "twitter-text";
import Parser from "html-react-parser";

// style Sheet
import "../css/style.css";

const twitterLogo = "https://image.flaticon.com/icons/svg/60/60580.svg";
const placeHolderImage =
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZFaNNQpYkXb48QDYdF8dTZi3Lu3Rx2fn_el5rfj0p0Bh5mDoWbA";
const instagramLogo =
  "https://kimberleymagain.com/wp-content/uploads/2017/11/instagram-logo-for-business-card-business-cards-copy-printing-and-screen-printing-in-winston.png";

const parseTweet = html =>
  Parser(twitter.autoLink(twitter.htmlEscape(html), { targetBlank: true }));

const InstaLink = tag =>{
  return (<a target="_blank" href={`https://www.instagram.com/explore/tags/${tag.tag}/`}>
    {" "}
    {`#${tag.tag}`}{" "}
  </a>)
};

const InstaHasher = ({ caption, tags } = item_data) => {
 caption = caption.substring(0, caption.indexOf("#") - 1);

  return (
    <p>
      {caption}
      {tags.map(v => <InstaLink key={v} tag={v} />)}
    </p>
  );
};

export const PostCard = ({ data, data: { service_slug } }) => {
  switch (service_slug) {
    case "manual":
      return <ManualCard {...data} />;
      break;
    case "twitter":
      return <TweetCard {...data} />;
      break;
    case "instagram":
      return <InstaCard {...data} />;
      break;
    default:
      return <div> unknown service </div>;
  }
};

export const InstaCard = props => {
  return (
    <div className="card-container">
      <div className="insta-logo">
        {" "}
        <img src={instagramLogo} width={"50px"} />{" "}
      </div>
      <div>
        {" "}
        <img
          className="image"
          src={props.item_data.image_url}
          onError={e => {
            e.target.src = placeHolderImage;
          }}
        />
      </div>
      <h4>{props.item_data.user.username} </h4>
      <div className="cardText"> <InstaHasher {...props.item_data} /> </div>
    </div>
  );
};

export const TweetCard = props => {
  return (
    <div className="card-container">
      <div className="logo">
        {" "}
        <img src={twitterLogo} width={"50px"} />{" "}
      </div>
      <h3>{props.item_data.user.username}</h3>
      <div className="cardText">{parseTweet(props.item_data.tweet)}</div>
      <a href={props.item_data.link}>
        {" "}
        <p>{props.item_data.link_text} </p>{" "}
      </a>
    </div>
  );
};

export const ManualCard = props => {
  return (
    <div className="card-container">
      <div className="aff-logo">
        {" "}
        <div className="aff-text"> AFF </div>{" "}
      </div>
      <div>
        {" "}
        <img
          className="image"
          src={props.item_data.image_url}
          onError={e => {
            e.target.src = placeHolderImage;
          }}
        />
      </div>
      <p className="cardText">{props.item_data.text} </p>
      <a target="_blank" href={props.item_data.link}>
        {" "}
        <p className="manual-link">{props.item_data.link_text} </p>{" "}
      </a>
    </div>
  );
};
