/////////
// App //
/////////
///
// This is the root of the main application that is rendered into the DOM at #content

// dependancies
import "babel-polyfill";
import React, { Component } from "react";
import { render } from "react-dom";
import PropTypes from "prop-types";
import axios from "axios";

//components
import {
  CSSGrid,
  layout,
  measureItems,
  makeResponsive,
  enterExitStyle,
  easings
} from "react-stonecutter";

const { quadIn } = easings;

//my components
import { PostCard } from "../js/cardComponent.jsx";

const URL = "http://private-cc77e-aff.apiary-mock.com/posts";

// style Sheet
import "../css/style.css";

class Content extends Component {
  constructor() {
    super();
    this.state = {
      fetching: false,
      filteredComponentData: []
    };
    this.filters = ["manual", "twitter", "instagram"];
    this.componentData = [];
    this.reloadCounter = 0;

    this.getMoreResults = this.getMoreResults.bind(this);
    this.filterResults = this.filterResults.bind(this);
    this.handleFilterPressed = this.handleFilterPressed.bind(this);
  }

  componentWillMount() {
    this.getMoreResults();
  }

  getMoreResults() {
    this.setState({ fetching: true });
    axios.get(URL).then(response => {
      this.componentData = [...this.componentData, ...response.data.items];
      this.setState({ fetching: false }, this.filterResults);
      ++this.reloadCounter;
    });
  }

  filterResults({ filters, componentData } = this) {
    this.setState({
      filteredComponentData: componentData
        .filter(v => filters.includes(v.service_slug))
        .sort((a, b) => {
          return Date(a.item_published) - Date(b.item_published);
        })
    });
  }

  handleFilterPressed({ target: { value } } = target) {
    if (this.filters.includes(value)) {
      this.filters = this.filters.filter(v => v !== value);
    } else {
      this.filters = [...this.filters, value];
    }
    this.filterResults();
  }

  render() {
    const Grid = makeResponsive(
      measureItems(CSSGrid, { measureImages: true }),
      {
        maxWidth: 1920,
        mixPadding: 0
      }
    );
    return (
      <div>
        <div className="title-bar">
          <button
            className={this.filters.includes("manual") ? "" : "filtered"}
            onClick={this.handleFilterPressed}
            value="manual"
          >
            AFF{" "}
          </button>
          <button
            className={this.filters.includes("twitter") ? "" : "filtered"}
            onClick={this.handleFilterPressed}
            value="twitter"
          >
            {" "}
            Twitter{" "}
          </button>
          <button
            className={this.filters.includes("instagram") ? "" : "filtered"}
            onClick={this.handleFilterPressed}
            value="instagram"
          >
            {" "}
            Instagram{" "}
          </button>
        </div>
        <div className="main">
          <div className="grid-container">
            <Grid
              className="grid"
              columns={6}
              columnWidth={300}
              gutterWidth={10}
              gutterHeight={10}
              layout={layout.pinterest}
              duration={800}
              easing={quadIn}
              enter={enterExitStyle.foldUp.enter}
              entered={enterExitStyle.foldUp.entered}
              exit={enterExitStyle.foldUp.exit}
            >
              {this.state.filteredComponentData.map(v => (
                <div key={v.item_source_id + Math.random().toString()}>
                  {" "}
                  <PostCard data={v} />{" "}
                </div>
              ))}
            </Grid>
          </div>
        </div>
        <div className={"footer"}>
          {(() => {
            if (this.state.fetching) {
              return (
                <button className="showMore" disabled>
                  {" "}
                  ...Please Wait{" "}
                </button>
              );
            } else {
              return (
                <button className="showMore" onClick={this.getMoreResults}>
                  {" "}
                  Show More{" "}
                </button>
              );
            }
          })()}
        </div>
      </div>
    );
  }
}

render(<Content />, document.getElementById("content"));
